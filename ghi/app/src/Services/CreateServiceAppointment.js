import React, {useState, useEffect} from 'react';;

const initialData = {
    date_time: "",
    reason: "",
    status: "Active",
    vin: "",
    customer: "",
    technician_id: "",
}

function CreateServiceAppointment() {
  const [CreatedAppointment, setCreatedAppointment] = useState();
  const [technicians, setTechnicians] = useState();
  const [formData, setFormData] = useState(initialData);
  const [errors, setErrors] = useState({});
  const [message, setMessage] = useState("");

  const getTechnitionData = async () => {
    // gets database information from docker server
    const techniciansurl = 'http://localhost:8080/api/technicians/';
    const response = await fetch(techniciansurl);
    if (response.ok) {
        const data = await response.json();
        setTechnicians(data.technicians);
    }
  }
  useEffect(() => {
    getTechnitionData();
  }, []);


  const handleSubmit = async (event) => {
    event.preventDefault();
    const newErrors = {};
    if (!formData.date_time) newErrors.date_time = 'date_time is required';
    if (!formData.customer) newErrors.customer = 'customer is required';
    if (!formData.vin) newErrors.vin = 'vin is required';
    if (!formData.status) newErrors.status = 'status is required';
    if (!formData.reason) newErrors.reason = 'reason is required';

    if (Object.keys(newErrors).length > 0) {
        setErrors(newErrors);
        return;
    }

    const appointmentsUrl = 'http://localhost:8080/api/appointments/';

    const fetchConfig = {
    method: "post",
    body: JSON.stringify(formData),
    headers: {
        'Content-Type': 'application/json',
    },
    };

    const response = await fetch(appointmentsUrl, fetchConfig);

    if (response.ok) {
      setFormData(initialData)
      setCreatedAppointment(true);
    }

    else {
        // Handle the error
        const errorMessage = await response.text();
        console.error('There was a problem with the fetch operation: ' + errorMessage);
    }

        // Set the message after form is submitted
        setMessage("Form submitted successfully!");
        // Clear the form fields
        setFormData(initialData);
    }

  const handleFormChange = (event) => {
      setFormData ({...formData,
      [event.target.name]: event.target.value
      })
  }

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Create a service appointment</h1>
            <form onSubmit={handleSubmit} id="create-appointment-form">
              <h5>Automobile VIN</h5>
              <div className="form-floating mb-3 ">
                  <input onChange={handleFormChange} value={formData.vin} placeholder="vin" required type="text" name="vin" id="vin" className="form-control" />
                  <label htmlFor="vin"></label>
                </div>
                <div className="form-floating mb-3">
                <h5>Customer Name</h5>
                  <input onChange={handleFormChange} value={formData.customer} placeholder="customer" required type="text" name="customer" id="customer" className="form-control" />
                  <label htmlFor="customer"></label>
                </div>
                <div className="form-floating mb-3">
                <h5>Reason for Appointment</h5>
                  <input onChange={handleFormChange} value={formData.reason} placeholder="reason" required type="text" name="reason" id="reason" className="form-control" />
                  <label htmlFor="reason"></label>
                </div>
                <div className="form-floating mb-3">
                <h5>Appointment Date and Time</h5>
                  <input onChange={handleFormChange} value={formData.date_time} placeholder="date_time" required type="datetime-local" name="date_time" id="date_time" className="form-control" />
                  <label htmlFor="date_time"></label>
                </div>
                <div className="form-floating mb-3">
                <h5>Technician</h5>
                  <select onChange={handleFormChange} value={formData.technician_id} placeholder="technician_id" required type="text" name="technician_id" id="technician_id" className="form-control" >
                  <option value=""></option>
                  {technicians && technicians.map(technician => {
                    return (<option key={technician.id} value={technician.id}>{technician.first_name} {technician.last_name}</option>)
                  })}
                    </select>
                    <label htmlFor="technician_id"></label>
                </div>
                <button className="btn btn-primary">Create Appointment</button>
            </form>
            {message && <p>{message}</p>}
          </div>
        </div>
      </div>
    </div>
  );
}

    export default CreateServiceAppointment;
