import { NavLink } from 'react-router-dom';
import React from 'react';
function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success">
      <div className="container-fluid">
        <NavLink className="navbar-brand" to="/">CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav me-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link" to="/">Home</NavLink>
            </li>
            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Inventory
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" to="/manufacturers">Manufacturers</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="/manufacturers/new">Add a manufacturer!</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="/models">Models</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="CreateVehicleModelForm">Create Vehicle Model</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="automobiles/">Automobiles</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="CreateAutomobileInventory">Create Automobile in Inventory</NavLink>
              </div>
            </li>

            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Sales
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" to="/salespeople">Salespeople</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="/salespeople/new">Add a salesperson!</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="/customers">Customers</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="/customers/new">Add a customer!</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="/sales">Sales List</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="/saleshistory">Sales History</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="/sales/new">Add a sale!</NavLink>
              </div>
            </li>

            <li className="nav-item dropdown">
              <a className="nav-link dropdown-toggle" href="#" id="navbarDropdown" role="button" data-bs-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Service
              </a>
              <div className="dropdown-menu" aria-labelledby="navbarDropdown">
                <NavLink className="dropdown-item" to="CreateServiceAppointment">Add an Appointment!</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="/technicians">Technicians</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="TechnicianCreateForm">Add a Technician!</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="ListAllServiceAppointments">All Service Requests</NavLink>
                <div className="dropdown-divider"></div>
                <NavLink className="dropdown-item" to="ServiceHistory">Service History</NavLink>
              </div>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
