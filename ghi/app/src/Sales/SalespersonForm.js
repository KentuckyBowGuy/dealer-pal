import React, {useState} from 'react';

function SalespersonForm() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [employeeId, setEmployeeId] = useState('');
    const [picture, setPicture] = useState('');


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.employee_id = employeeId;
        data.picture_url = picture;

        const salespersonUrl = 'http://localhost:8090/api/salespeople/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(salespersonUrl, fetchConfig);
        if (response.ok) {
            const newSalesperson = await response.json();
            console.log(newSalesperson);
            setFirstName('');
            setLastName('');
            setEmployeeId('');
            setPicture('');

        } else if (response.status === 400) {
            const error = await response.json();
            console.log(error);
        }
    }


    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }


    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }


    const handleEmployeeIdChange = (event) => {
        const value = event.target.value;
        setEmployeeId(value);
    }


    const handlePictureChange = (event) => {
        const value = event.target.value;
        setPicture(value);
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new salesperson!</h1>
                    <form onSubmit={handleSubmit} id="create-salesperson-form">
                        <div className="form-floating mb-3">
                            <input onChange = {handleFirstNameChange} value = {firstName} placeholder="First name" required type="text" name="first_name" id="first_name" className="form-control" />
                            <label htmlFor="first_name">Salesperson's first name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange = {handleLastNameChange} value = {lastName} placeholder="Last name" required type="text" name="last_name" id="last_name" className="form-control" />
                            <label htmlFor="clast_name">Salesperson's last name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange = {handleEmployeeIdChange} value = {employeeId} placeholder="Employee Id" required type="text" name="employee_id" id="employee_id" className="form-control" />
                            <label htmlFor="employee_id">Employee ID</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange = {handlePictureChange} value = {picture} placeholder="Picture" type="url" name="picture_url" id="picture_url" className="form-control" />
                            <label htmlFor="picture_url">Picture</label>
                        </div>
                        <button className="btn btn-dark">Add to team!</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SalespersonForm;
