import React, { useEffect, useState } from "react";
function ShowCustomer() {
    const [customers, setCustomers] = useState([]);
    const [selectedCustomer, setSelectedCustomer] = useState(null);


    const getCustomers = async () => {
        const customersUrl = 'http://localhost:8090/api/customers/';
        const response = await fetch(customersUrl);
        if (response.ok) {
            const customerData = await response.json();

            if (customerData === undefined) {
                return null;
            }

            setCustomers(customerData.customers);
            console.log(customerData)
        } else {
            console.log('error');
        }
    }


    useEffect(() => {
        getCustomers();
    }, []);


    const handleCustomerClick = (customer) => {
        setSelectedCustomer(customer);
    }


    const handleResetClick = async () => {
        const customersUrl = 'http://localhost:8090/api/customers/';
        const response = await fetch(customersUrl);
        if (response.ok) {
            const customerData = await response.json();

            if (customerData === undefined) {
                return null;
            }
            setCustomers(customerData.customers);
            setSelectedCustomer(null);
        }
    }


    const handleDeleteClick = async () => {
        if (selectedCustomer) {
            const deleteUrl = `http://localhost:8090/api/customers/${selectedCustomer.id}`;
            const response = await fetch(deleteUrl, {
                method: 'DELETE'
            });
            const customersUrl = 'http://localhost:8090/api/customers/';
            const responseTwo = await fetch(customersUrl);
            if (response.ok) {
                const customerData = await responseTwo.json();


                setCustomers(customerData.customers);
                setSelectedCustomer(null);
            }
        }
    }


    const handleUpdateClick = async () => {
        if (selectedCustomer) {
          window.location.href = `/customers/${selectedCustomer.id}`;
        }
      }


    return (
        <div>
            <h1 className="fw-bold shadow w-25 p-3 mb-5 bg-white rounded">Customers</h1>
            <table className="table table-hover">
                <thead>
                    <tr className="table-success">
                        <th className="fs-3" scope="col">First Name</th>
                        <th className="fs-3" scope="col">Last Name</th>
                        <th className="fs-3" scope="col">Address</th>
                        <th className="fs-3" scope="col">Phone number</th>
                    </tr>
                </thead>
                <tbody>
                    {customers && customers.map(customer => (
                        <tr
                            className={selectedCustomer === customer ? "table-primary" : ""}
                            key={customer.id}

                            onClick={() => handleCustomerClick(customer)}
                        >
                            <td>{customer.first_name}</td>
                            <td>{customer.last_name}</td>
                            <td>{customer.address}</td>
                            <td>{customer.phone_number}</td>
                        </tr>
                    ))}
                </tbody>
            </table>

            {selectedCustomer && (
                <div>
                    <h2>Selected Customer Details:</h2>
                    <p>First Name: {selectedCustomer.first_name}</p>
                    <p>Last Name: {selectedCustomer.last_name}</p>
                    <p>Address: {selectedCustomer.address}</p>
                    <p>Phone Number: {selectedCustomer.phone_number}</p>

                    <div/>
                    <div className="d-flex justify-content-start gap-2 ">

                    <button className="btn btn-danger " onClick={handleDeleteClick}>Delete Customer</button>
                    <button className="btn btn-warning" onClick={handleUpdateClick}>Update a customer</button>
                    <button className="btn btn-primary" onClick={handleResetClick}>Reset</button>


                    </div>
                </div>
            )}
        </div>
    );
}

export default ShowCustomer;
