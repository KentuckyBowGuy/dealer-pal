import React, { useEffect, useState } from "react";

function ShowSale() {
    const [sales, setSales] = useState([]);
    const [selectedSale, setSelectedSale] = useState(null);
    const [selectedSalesperson, setSelectedSalesperson] = useState(null);
    const [salespersons, setSalespersons] = useState([]);


    const getSales = async () => {
        const salesUrl = 'http://localhost:8090/api/sales/';
        const response = await fetch(salesUrl);
        if (response.ok) {
            const saleData = await response.json();

            if (saleData === undefined) {
                return null;
            }

            setSales(saleData.sales);
            setSalespersons(saleData.salespersons);
            console.log(saleData);
        } else {
            console.log('error');
        }
    }

    useEffect(() => {
        getSales();
    }, []);


    const handleSalespersonChange = (event) => {
        const selectedSalesperson = event.target.value;
        setSelectedSalesperson(selectedSalesperson);
        const filteredSales = sales.filter(sale => sale.salesperson === selectedSalesperson);
        setSales(filteredSales);
        setSelectedSale(null);
    }


    const handleResetSalespersonClick = async () => {
        const salesUrl = 'http://localhost:8090/api/sales/';
        const response = await fetch(salesUrl);
        if (response.ok) {
            const saleData = await response.json();

            if (saleData === undefined) {
                return null;
            }
            setSales(saleData.sales);
            setSelectedSale(null);

    }
}


    return (
        <div><h1 className="fw-bold shadow w-25 p-3 mb-5 bg-white rounded">Sales History</h1>
            <label htmlFor="salesperson">Select Salesperson:</label>
            <div>
                <select className="form-select form-select-lg mb-3" aria-label="Large select example" id="salesperson" onChange={handleSalespersonChange}>
                    <option>Select Salesperson</option>
                    {
                        [...new Set([
                            ...(salespersons || []).map(salesperson => salesperson.name),
                            ...(sales || []).map(sale => sale.salesperson)
                        ])].map(name => <option key={name} value={name}>{name}</option>)
                    }
                </select>
            </div>
            <div><button className="btn btn-primary" onClick={handleResetSalespersonClick}>Reset Salesperson List</button></div>

            <table className="table table-hover">
                <thead>
                    <tr className="table-success">
                        <th className="fs-3" scope="col">Price</th>
                        <th className="fs-3" scope="col">Automobile VIN</th>
                        <th className="fs-3" scope="col">Salesperson</th>
                        <th className="fs-3" scope="col">Customer</th>
                    </tr>
                </thead>
                <tbody>
                    {sales && sales.map(sale => (
                        <tr
                            className={selectedSale === sale ? "table-primary" : ""}
                            key={sale.id}
                        >
                            <td>${sale.price}</td>
                            <td>{sale.automobile}</td>
                            <td>{sale.salesperson}</td>
                            <td>{sale.customer}</td>
                        </tr>
                    ))}
                </tbody>
            </table>
        </div>
    );
}

export default ShowSale;
