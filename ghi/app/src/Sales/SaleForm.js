import React, { useEffect, useState} from 'react';

function SaleForm() {
    const [automobiles, setAutomobiles] = useState([]);
    const [salespeople, setSalespeople] = useState([]);
    const [customers, setCustomers] = useState([]);
    const [price, setPrice] = useState('');
    const [automobile, setAutomobile] = useState('');
    const [salesperson, setSalesperson] = useState('');
    const [customer, setCustomer] = useState('');
    const [automobileVin, setAutomobileVin] = useState('');

    const handlePriceChange = (event) => {
        const value = event.target.value;
        setPrice(value);
    }

    const handleAutomobileChange = (event) => {
        const value = event.target.value;
        setAutomobile(value);
        const selectedAutomobile = automobiles.find(automobile => automobile.href === event.target.value);
        if (selectedAutomobile) {
            setAutomobileVin(selectedAutomobile.vin);
        }
    }


    const handleSalespersonChange = (event) => {
        const value = event.target.value;
        setSalesperson(value);
    }


    const handleCustomerChange = (event) => {
        const value = event.target.value;
        setCustomer(value);
    }


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.price = price;
        data.automobile = event.target.automobile.value;
        data.salesperson = event.target.salesperson.value;
        data.customer = event.target.customer.value;

        const saleUrl = 'http://localhost:8090/api/sales/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };
        const response = await fetch(saleUrl, fetchConfig);
        if (response.ok) {
            const newSale = await response.json();
            console.log(newSale);
            setPrice('');
            setAutomobile('');
            setSalesperson('');
            setCustomer('');

            const automobileUrl = `http://localhost:8100/api/automobiles/${automobileVin}/`;
            const automobileResponse = await fetch(automobileUrl, {
                method: 'PUT',
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify({ sold: true })
            });

            if (automobileResponse.ok) {
                fetchAutoData();
            } else {
                console.error('Failed to update automobile sold status');
            }
        } else if (response.status === 400) {
            const error = await response.json();
            console.log(error);
        }
    }


    const fetchAutoData = async () => {
        const automobileUrl = 'http://localhost:8100/api/automobiles/';
        const response = await fetch(automobileUrl);
        if (response.ok) {
            const automobileData = await response.json();
            console.log(automobileData);
            const filteredAutomobiles = automobileData.autos.filter(automobile => automobile.sold === false);
            setAutomobiles(filteredAutomobiles);
        }
    }

    useEffect(() => {
        fetchAutoData();
    }, []);


    const fetchSalespersonData = async () => {
        const salespersonUrl = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(salespersonUrl);
        if (response.ok) {
            const salespersonData = await response.json();
            console.log(salespersonData);
            setSalespeople(salespersonData.salespeople);
        }
    }

    useEffect(() => {
        fetchSalespersonData();
    }, []);


    const fetchCustomerData = async () => {
        const customerUrl = 'http://localhost:8090/api/customers/';
        const response = await fetch(customerUrl);
        if (response.ok) {
            const customerData = await response.json();
            console.log(customerData);
            setCustomers(customerData.customers);
        }
    }

    useEffect(() => {
        fetchCustomerData();
    }, []);


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new sale!</h1>
                    <form onSubmit={handleSubmit} id="create-sale-form">
                    <div className="mb-3">
                            <select onChange = {handleAutomobileChange} value={automobile} required name="automobile" id="automobile" className="form-select">
                                <option >Please select an automobile</option>
                                {automobiles.map(automobile => (
                                    <option key={automobile.href} value={automobile.href}>
                                        {automobile.vin}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange = {handlePriceChange} value = {price} placeholder="Price" type="float" name="price" id="price" className="form-control" />
                            <label htmlFor="price">Price</label>
                        </div>
                        <div className="mb-3">
                            <select onChange = {handleSalespersonChange} value={salesperson} required name="salesperson" id="salesperson" className="form-select">
                                <option >Please select a salesperson</option>
                                {salespeople.map(salesperson => (
                                    <option key={salesperson.id} value={salesperson.id}>
                                        {salesperson.first_name} {salesperson.last_name}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <div className="mb-3">
                            <select onChange = {handleCustomerChange} value={customer} required name="customer" id="customer" className="form-select">
                                <option >Please select a customer</option>
                                {customers.map(customer => (
                                    <option key={customer.id} value={customer.id}>
                                        {customer.first_name} {customer.last_name}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <button className="btn btn-dark">Complete sale!</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default SaleForm;
