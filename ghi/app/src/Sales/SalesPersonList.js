import React, { useEffect, useState } from "react";
function ShowSalesperson() {

    const [salespeople, setSalespeople] = useState([]);
    const [selectedSalesperson, setSelectedSalesperson] = useState(null);


    const getSalespeople = async () => {
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(salespeopleUrl);
        if (response.ok) {
            const salespersonData = await response.json();

            if (salespersonData === undefined) {
                return null;
            }

            setSalespeople(salespersonData.salespeople);
            console.log(salespersonData)
        } else {
            console.log('error');
        }
    }

    useEffect(() => {
        getSalespeople();
    }, []);


    const handleSalespersonClick = (salesperson) => {
        setSelectedSalesperson(salesperson);
    }


    const handleResetClick = async () => {
        const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
        const response = await fetch(salespeopleUrl);
        if (response.ok) {
            const salespersonData = await response.json();

            if (salespersonData === undefined) {
                return null;
            }
            setSalespeople(salespersonData.salespeople);
            setSelectedSalesperson(null);
        }
    }


    const handleDeleteClick = async () => {
        if (selectedSalesperson) {
            const deleteUrl = `http://localhost:8090/api/salespeople/${selectedSalesperson.id}`;
            const response = await fetch(deleteUrl, {
                method: 'DELETE'
            });
            const salespeopleUrl = 'http://localhost:8090/api/salespeople/';
            const responseTwo = await fetch(salespeopleUrl);
            if (response.ok) {
                const salespersonData = await responseTwo.json();


                setSalespeople(salespersonData.salespeople);
                setSelectedSalesperson(null);
            }
        }
    }


    const handleUpdateClick = async () => {
        if (selectedSalesperson) {
          window.location.href = `/salespeople/${selectedSalesperson.id}`;
        }
      }


      const handlePhotoNull = () => {
        const imgSrc = selectedSalesperson.picture_url === '' ? "https://t4.ftcdn.net/jpg/03/08/68/19/240_F_308681935_VSuCNvhuif2A8JknPiocgGR2Ag7D1ZqN.jpg" : selectedSalesperson.picture_url;
        return <img className="list-image" src={imgSrc} alt="" />;
      };


    return (
        <div>
            <h1 className="fw-bold shadow w-25 p-3 mb-5 bg-white rounded">Salespeople</h1>
            <table className="table table-hover">
                <thead>
                    <tr className="table-success">
                        <th className="fs-3" scope="col">First Name</th>
                        <th className="fs-3" scope="col">Last Name</th>
                        <th className="fs-3" scope="col">Employee ID</th>
                    </tr>
                </thead>
                <tbody>
                    {salespeople && salespeople.map(salesperson => (
                        <tr
                            className={selectedSalesperson === salesperson ? "table-primary" : ""}
                            key={salesperson.id}

                            onClick={() => handleSalespersonClick(salesperson)}
                        >
                            <td>{salesperson.first_name}</td>
                            <td>{salesperson.last_name}</td>
                            <td>{salesperson.employee_id}</td>
                        </tr>
                    ))}
                </tbody>
            </table>

            {selectedSalesperson && (
                <div>
                    <h2>Selected Salesperson Details:</h2>
                    <p>First Name: {selectedSalesperson.first_name}</p>
                    <p>Last Name: {selectedSalesperson.last_name}</p>
                    <p>Employee ID: {selectedSalesperson.employee_id}</p>
                    <p>{handlePhotoNull()}</p>

                    <div/>
                    <div className="d-flex justify-content-start gap-2 ">

                    <button className="btn btn-danger " onClick={handleDeleteClick}>Delete Salesperson</button>
                    <button className="btn btn-warning" onClick={handleUpdateClick}>Update a salesperson</button>
                    <button className="btn btn-primary" onClick={handleResetClick}>Reset</button>


                    </div>
                </div>
            )}
        </div>
    );
}

export default ShowSalesperson;
