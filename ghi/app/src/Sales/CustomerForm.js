import React, {useState} from 'react';

function CustomerForm() {
    const [firstName, setFirstName] = useState('');
    const [lastName, setLastName] = useState('');
    const [address, setAddress] = useState('');
    const [phoneNumber, setPhoneNumber] = useState('');


    const handleSubmit = async (event) => {
        event.preventDefault();
        const data = {};
        data.first_name = firstName;
        data.last_name = lastName;
        data.address = address;
        data.phone_number = phoneNumber;

        const customerUrl = 'http://localhost:8090/api/customers/';
        const fetchConfig = {
            method: 'POST',
            body: JSON.stringify(data),
            headers: {
                'Content-Type': 'application/json',
            },
        };

        const response = await fetch(customerUrl, fetchConfig);
        if (response.ok) {
            const newCustomer = await response.json();
            console.log(newCustomer);
            setFirstName('');
            setLastName('');
            setAddress('');
            setPhoneNumber('');

        } else if (response.status === 400) {
            const error = await response.json();
            console.log(error);
        }
    }


    const handleFirstNameChange = (event) => {
        const value = event.target.value;
        setFirstName(value);
    }


    const handleLastNameChange = (event) => {
        const value = event.target.value;
        setLastName(value);
    }


    const handleAddressChange = (event) => {
        const value = event.target.value;
        setAddress(value);
    }


    const handlePhoneNumberChange = (event) => {
        const value = event.target.value;
        setPhoneNumber(value);
    }


    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Add a new customer!</h1>
                    <form onSubmit={handleSubmit} id="create-customer-form">
                        <div className="form-floating mb-3">
                            <input onChange = {handleFirstNameChange} value = {firstName} placeholder="First name" required type="text" name="first_name" id="first_name" className="form-control" />
                            <label htmlFor="first_name">Customer's first name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange = {handleLastNameChange} value = {lastName} placeholder="Last name" required type="text" name="last_name" id="last_name" className="form-control" />
                            <label htmlFor="clast_name">Customer's last name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange = {handleAddressChange} value = {address} placeholder="Address" required type="text" name="address" id="address" className="form-control" />
                            <label htmlFor="address">Address</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input onChange = {handlePhoneNumberChange} value = {phoneNumber} placeholder="Phone number" type="text" name="phone_number" id="phone_number" className="form-control" />
                            <label htmlFor="phone_number">Phone number</label>
                        </div>
                        <button className="btn btn-dark">Add to customer list!</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default CustomerForm;
