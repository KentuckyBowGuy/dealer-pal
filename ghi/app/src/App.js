import { Routes, Route } from 'react-router-dom';
import MainPage from './MainPage';
import Nav from './Nav';
import SalespersonList from './Sales/SalesPersonList';
import CustomerList from './Sales/CustomerList';
import SalespersonForm from './Sales/SalespersonForm';
import CustomerForm from './Sales/CustomerForm';
import SaleForm from './Sales/SaleForm';
import SalesList from './Sales/SalesList';
import SalesHistory from './Sales/SalesHistory';
import ManufacturerList from './Inventory/ManufacturerList';
import ManufacturerForm from './Inventory/ManufacturerForm';
import ModelList from './Inventory/ModelList';
import TechnicianList from './Technicians/TechnicianList';
import TechnicianCreateForm from './Technicians/TechnicianCreateForm';
import CreateServiceAppointment from './Services/CreateServiceAppointment';
import AutomobilesInventoryList from './Inventory/AutomobilesInventoryList';
import CreateAutomobileInventory from './Inventory/CreateAutomobileInventory';
import CreateVehicleModelForm from './Inventory/CreateVehicleModelForm';
import CustomerUpdate from './Sales/CustomerUpdate';
import SalespersonUpdate from './Sales/SalespersonUpdate';
import ServiceHistory from './Services/ServiceHistory';
import ListAllServiceAppointments from './Services/ListAllServiceAppointments';






function App() {
  return (
    <>
      <Nav />
      <div className="container">
        <Routes>
          <Route path="/" element={<MainPage />} />
          <Route path="/salespeople" element={<SalespersonList />} />
          <Route path="/customers" element={<CustomerList />} />
          <Route path="/salespeople/new" element={<SalespersonForm />} />
          <Route path="/customers/new" element={<CustomerForm />} />
          <Route path="/sales/new" element={<SaleForm />} />
          <Route path="/sales" element={<SalesList />} />
          <Route path="/saleshistory" element={<SalesHistory />} />
          <Route path="/manufacturers" element={<ManufacturerList />} />
          <Route path="/manufacturers/new" element={<ManufacturerForm />} />
          <Route path="/models" element={<ModelList />} />
          <Route path="technicians/" element={<TechnicianList />} />
          <Route path="TechnicianCreateForm" element={<TechnicianCreateForm />} />
          <Route path="CreateServiceAppointment" element={<CreateServiceAppointment />} />
          <Route path="ListAllServiceAppointments" element={<ListAllServiceAppointments />} />
          <Route path="ServiceHistory" element={<ServiceHistory />} />
          <Route path="automobiles/" element={<AutomobilesInventoryList />} />
          <Route path="CreateAutomobileInventory" element={<CreateAutomobileInventory />} />
          <Route path="CreateVehicleModelForm" element={<CreateVehicleModelForm />} />
          <Route path="/customers/:id" element={<CustomerUpdate/>} />
          <Route path="/salespeople/:id" element={<SalespersonUpdate/>} />
        </Routes>
      </div>
    </>
  );
}

export default App;
