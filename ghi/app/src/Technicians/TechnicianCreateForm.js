import React, {useState, useEffect} from 'react';;

// this data if from the model encorder for technician
const initialData = {
  first_name: "", 
  last_name: "",
  employee_id: "",
}

function TeachnicianCreateForm() {
  const [CreatedTechnician, setCreatedTechnician] = useState(false);
  const [technicians, setTechnicians] = useState([]);
  const [formData, setFormData] = useState(initialData);
  const [errors, setErrors] = useState({});
  const [message, setMessage] = useState("");



  const getTechnitionData = async () => {
    // gets database information from docker server
    const techniciansurl = 'http://localhost:8080/api/technicians/';
    const response = await fetch(techniciansurl);
    if (response.ok) {
      const data = await response.json();
      setTechnicians(data.technicians);
    }
  }  
  useEffect(() => {
    getTechnitionData();
  }, []);

  const handleSubmit = async (event) => {
    event.preventDefault();
    
    const newErrors = {};
    if (!formData.first_name) newErrors.first_name = 'first_name is required';
    if (!formData.last_name) newErrors.last_name = 'last_name is required';
    if (!formData.employee_id) newErrors.employee_id = 'employee_id is required';      

    if (Object.keys(newErrors).length > 0) {
        setErrors(newErrors);
        return;
    }

  const techniciansUrl = 'http://localhost:8080/api/technicians/';

  const fetchConfig = {
    method: "post",
    body: JSON.stringify(formData),
    headers: {
      'Content-Type': 'application/json',
    },
  };

  const response = await fetch(techniciansUrl, fetchConfig);

  if (response.ok) {
  setFormData(initialData)
  setCreatedTechnician(true);
  }
  // Set the message after form is submitted
  setMessage("Form submitted successfully!");
  // Clear the form fields
  setFormData(initialData);  
  } 

  const handleFormChange = (event) => {
    setFormData ({...formData, 
      [event.target.name]: event.target.value
    })
  }

  // CSS classes for rendering
  let dropdownClasses = 'form-select d-none';
  if (technicians && technicians.length > 0) {
    dropdownClasses = 'form-select';
  }

  return (
    <div className="container">
      <div className="row">
        <div className="offset-3 col-6">
          <div className="shadow p-4 mt-4">
            <h1>Add a technician</h1>
            <form onSubmit={handleSubmit} id="create-technician-form">
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.first_name} placeholder="first_name" required type="text" name="first_name" id="first_name" className="form-control" />
                <label htmlFor="first_name">First name...</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.last_name} placeholder="last_name" required type="text" name="last_name" id="last_name" className="form-control" />
                <label htmlFor="last_name">Last name...</label>
              </div>
              <div className="form-floating mb-3">
                <input onChange={handleFormChange} value={formData.employee_id} placeholder="employee_id" required type="text" name="employee_id" id="employee_id" className="form-control" />
                <label htmlFor="employee_id">Employee ID...</label>
              </div>
              <button className="btn btn-primary">Create Technician</button>
            </form>
            {message && <p>{message}</p>}
          </div>
        </div>
      </div>
    </div>
  );
}

export default TeachnicianCreateForm;