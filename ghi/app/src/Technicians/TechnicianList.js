import React, { useEffect, useState } from 'react';

function TechnicianList() {
    const [technicans, setTechnicians] = useState([]);
    const [error, setError] = useState(null);
    const [message, setMessage] = useState("");

    const getTechniciansData = async () => {
        try {
            const response = await fetch('http://localhost:8080/api/technicians/');
            if (response.ok) {
                const data = await response.json();
                setTechnicians(data.technicians);
            } else {
                setError('Failed to fetch technicans data');
            }
        } catch (error) {
            setError('An error occurred while fetching technicans data');
        }
    };

    useEffect(() => {
        getTechniciansData();
    }, []);

    if (error) {
        return <div>Error: {error}</div>;
    }

    const handleDelete = async (id) => {
        const request = await fetch(`http://localhost:8080/api/technicians/${id}`, { method: "DELETE"});
        const resp = await request.json();
        getTechniciansData();
        setMessage("Technician deleted successfully!");
    }

    return (
        <div>
            <h1>Technicians</h1>
            <table className="table table-striped border-3">
                <thead>
                    <tr>
                        <th>First Name</th>
                        <th>Last Name</th>
                        <th>Employee ID</th>
                        {/* <th>Technician Picture</th> */}
                    </tr>
                </thead>
                <tbody>
                    {technicans.map(technician => {
                        return (
                            <tr key={technician.employee_id}>
                                <td>{technician.first_name}</td>
                                <td>{technician.last_name}</td>
                                <td>{technician.employee_id}</td>
                                {/* <td>
                                    <img src={technician.hat_picture_url} alt = {technician.first_name} width = "50" height="50"></img>
                                </td> */}
                                {/* <td><button onClick={()=> { handleDelete(technician.employee_id)
                                }} className="btn btn-danger">Delete</button></td> */}
                            </tr>
                        );
                    })}
                </tbody>
            </table>
        </div>
    );
}

export default TechnicianList;
