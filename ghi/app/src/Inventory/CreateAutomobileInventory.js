import React, { useEffect, useState } from 'react';

function AutomobileForm() {
  const [models, setModels] = useState([]);
  const [color, setColor] = useState('');
  const [year, setYear] = useState(0);
  const [vin, setVin] = useState('');
  const [model, setModel] = useState();

  const handleSubmit = async (event) => {
    event.preventDefault();
    const data = {};
    data.color = color;
    data.year = year;
    data.vin = vin;
    data.model_id = event.target.model.value;

    const automobileUrl = 'http://localhost:8100/api/automobiles/';
    const fetchConfig = {
      method: 'POST',
      body: JSON.stringify(data),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(automobileUrl, fetchConfig);
    if (response.ok) {
      const newAutomobile = await response.json();
      console.log(newAutomobile);
      setColor('');
      setYear(0);
      setVin('');
      setModel('');

    } else if (response.status === 400) {
      const error = await response.json();
      console.log(error);
    }
  };

  const handleColorChange = (event) => {
    const value = event.target.value;
    setColor(value);
  };

  const handleYearChange = (event) => {
    const value = event.target.value;
    setYear(value);
  };

  const handleVinChange = (event) => {
    const value = event.target.value;
    setVin(value);
  };

  const handleModelChange = (event) => {
    const value = event.target.value;
    setModel(value);
  };

  const fetchModelData = async () => {
    const modelUrl = 'http://localhost:8100/api/models/';
    const response = await fetch(modelUrl);
    if (response.ok) {
      const modelData = await response.json();
      setModels(modelData.models);
    } else {
      console.error('Failed to get model data');
    }
  };

  useEffect(() => {
    fetchModelData();
  }, []);

  return (
    <div className="row">
      <div className="offset-3 col-6">
        <div className="shadow p-4 mt-4">
          <h1>Add an automobile!</h1>
          <form onSubmit={handleSubmit} id="create-automobile-form">
            <div className="form-floating mb-3">
              <input
                onChange={handleYearChange}
                value={year}
                placeholder="Year"
                required
                type="number"
                name="year"
                id="year"
                className="form-control"
              />
              <label htmlFor="year">Year</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleColorChange}
                value={color}
                placeholder="Color"
                required
                type="text"
                name="color"
                id="color"
                className="form-control"
              />
              <label htmlFor="color">Color</label>
            </div>
            <div className="form-floating mb-3">
              <input
                onChange={handleVinChange}
                value={vin}
                placeholder="Vin"
                required
                type="text"
                name="vin"
                id="vin"
                className="form-control"
              />
              <label htmlFor="vin">VIN Number</label>
            </div>
            <div className="mb-3">
              <select
                onChange={handleModelChange}
                value={model}
                required
                name="model"
                id="model"
                className="form-select"
              >
                <option>Please select a model</option>
                {models && models.map((model) => (
                <option key={model.id} value={model.id}>{model.name}
                </option>
                ))}
              </select>
            </div>
            <button className="btn btn-dark">Add to lot!</button>
          </form>
        </div>
      </div>
    </div>
  );
}

export default AutomobileForm;
