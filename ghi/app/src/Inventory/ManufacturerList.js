import React, { useEffect, useState } from "react";
function ShowManufacturer() {
    const [manufacturers, setManufacturers] = useState([]);
    const [selectedManufacturer, setSelectedManufacturer] = useState(null);


    const getManufacturers = async () => {
        const manufacturersUrl = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(manufacturersUrl);
        if (response.ok) {
            const manufacturerData = await response.json();

            if (manufacturerData === undefined) {
                return null;
            }

            setManufacturers(manufacturerData.manufacturers);
            console.log(manufacturerData)
        } else {
            console.log('error');
        }
    }

    useEffect(() => {
        getManufacturers();
    }, []);


    const handleManufacturerClick = (manufacturer) => {
        setSelectedManufacturer(manufacturer);
    }


    const handleResetClick = async () => {
        const manufacturersUrl = 'http://localhost:8100/api/manufacturers/';
        const response = await fetch(manufacturersUrl);
        if (response.ok) {
            const manufacturerData = await response.json();

            if (manufacturerData === undefined) {
                return null;
            }
            setManufacturers(manufacturerData.manufacturers);
            setSelectedManufacturer(null);
        }
    }


    const handleDeleteClick = async () => {
        if (selectedManufacturer) {
            const deleteUrl = `http://localhost:8100/api/manufacturers/${selectedManufacturer.id}/`;
            const response = await fetch(deleteUrl, {
                method: 'DELETE'
            });
            const manufacturersUrl = 'http://localhost:8100/api/manufacturers/';
            const responseTwo = await fetch(manufacturersUrl);
            if (response.ok) {
                const manufacturerData = await responseTwo.json();


                setManufacturers(manufacturerData.manufacturers);
                setSelectedManufacturer(null);
            }
        }
    }


    return (
        <div>
            <h1 className="fw-bold shadow w-25 p-3 mb-5 bg-white rounded">Manufacturers</h1>
            <table className="table table-hover">
                <thead>
                    <tr className="table-success">
                        <th className="fs-3" scope="col">Name</th>
                        <th className="fs-3" scope="col">ID</th>
                    </tr>
                </thead>
                <tbody>
                    {manufacturers && manufacturers.map(manufacturer => (
                        <tr
                            className={selectedManufacturer === manufacturer ? "table-primary" : ""}
                            key={manufacturer.id}

                            onClick={() => handleManufacturerClick(manufacturer)}
                        >
                            <td className ="fw-bold">{manufacturer.name}</td>
                            <td>{manufacturer.id}</td>
                        </tr>
                    ))}
                </tbody>
            </table>

            {selectedManufacturer && (
                <div>
                    <h2>Selected Manufacturer Details:</h2>
                    <p>Price: {selectedManufacturer.name}</p>
                    <p>Automobile VIN: {selectedManufacturer.id}</p>
                    <div/>
                    <div className="d-flex justify-content-start gap-2 ">

                    <button className="btn btn-danger " onClick={handleDeleteClick}>Delete Manufacturer</button>
                    <button className="btn btn-primary" onClick={handleResetClick}>Reset</button>


                    </div>
                </div>
            )}
        </div>
    );
}

export default ShowManufacturer;
