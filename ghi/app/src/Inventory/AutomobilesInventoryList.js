import React, { useEffect, useState } from 'react';

function AutomobilesInventoryList() {
    const [automobiles, setAutomobiles] = useState();
    const [error, setError] = useState();
    const [message, setMessage] = useState("");

    const getAutomobilesData = async () => {
        try {
            const response = await fetch('http://localhost:8100/api/automobiles/');
            if (response.ok) {
                const data = await response.json();
                setAutomobiles(data.autos);
            } else {
                setError('Failed to fetch automobiles data');
            }
        } catch (error) {
            setError('An error occurred while fetching automobiles data');
        }
    };

    useEffect(() => {
        getAutomobilesData();
    }, []);

    if (error) {
        return <div>Error: {error}</div>;
    }

    return (
        <div>
            <h1>Automobiles</h1>
            <table className="table table-striped">
                <thead>
                    <tr>
                        <th>Manufacturer</th>
                        <th>Vehicle Model</th>
                        <th>Year</th>
                        <th>Color</th>
                        <th>VIN</th>
                        <th>Sold</th>
                    </tr>
                </thead>
                <tbody>
                    {automobiles && automobiles.map(automobile => {
                        return(
                            <tr key={automobile.id}>
                                <td>{automobile.model.manufacturer.name}</td>
                                <td>{automobile.model.name}</td>
                                <td>{automobile.year}</td>
                                <td>{automobile.color}</td>
                                <td>{automobile.vin}</td>
                                <td>{automobile.sold ? 'Yes' : 'No'}</td>
                                
                            </tr>
                        )
                    })
                    }
                </tbody>
            </table>
        </div>
    );
}

export default AutomobilesInventoryList;
