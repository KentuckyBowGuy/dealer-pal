from django.urls import path

from .views import list_technicians, show_technicians, list_appointments, show_appointments

urlpatterns = [
    path("technicians/", list_technicians, name="list_technicians"),
    path("technicians/<int:pk>/", show_technicians, name="show_technicians"),
    path("appointments/", list_appointments, name="list_appointments"),
    path("appointments/<int:pk>/", show_appointments, name="show_appointments")
    
]
