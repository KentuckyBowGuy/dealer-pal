from common.json import ModelEncoder
from .models import AutomobileVO, Technician, Appointment


class AutomobileVODetailEncoder(ModelEncoder):
    model = AutomobileVO
    properties = [
        "id",
        "vin ", 
        "sold", 
        "import_href"]


class TechnicianModelEncoder(ModelEncoder):
    """
    Encoder for Technician model.
    """
    model = Technician
    properties = [
        "id",
        "first_name",
        "last_name",
        "employee_id"
    ]


class AppointmentModelEncoder(ModelEncoder):
    model = Appointment
    properties = [
        "id",
        "date_time",
        "reason",
        "status",
        "customer",
        "technician",
    ]

    encoders = {
        "technician": TechnicianModelEncoder(),
    }

    def get_extra_data(self, o):
        return {
            "technician": o.technician.first_name + " " + o.technician.last_name,
        }