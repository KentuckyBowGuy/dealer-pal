import json
from django.http import JsonResponse, HttpResponse
from django.views.decorators.http import require_http_methods
from .models import Technician, Appointment
from .encoders import (TechnicianModelEncoder, AppointmentModelEncoder)


@require_http_methods(["GET", "POST"])
def list_technicians(request):
    if request.method == "GET":
        technicians = Technician.objects.all()
        return JsonResponse(
            {"technicians": technicians},
            encoder=TechnicianModelEncoder,
        )
    else:
        try:
            content = json.loads(request.body)
            technicians = Technician.objects.create(**content)
            return JsonResponse(
                technicians,
                encoder=TechnicianModelEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Cannot create technician"},
                                status=404)
@require_http_methods(["GET",  "DELETE", "PUT"])
def show_technicians(request, pk):
    if request.method == "GET":
        try:
            technician = Technician.objects.get(id=pk)
            return JsonResponse(
                technician,
                encoder=TechnicianModelEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            response = JsonResponse({"message": "Technician Does not exist"})
            response.status_code = 404
            return response
    elif request.method == "DELETE":
        try:
            technician = Technician.objects.get(id=pk)
            technician.delete()
            return JsonResponse({"message": "technician successfully deleted."})
        except Technician.DoesNotExist:
            return JsonResponse({"message": "technician does not exist"}, status=400)
    else:  # PUT
        try:
            content = json.loads(request.body)
            technician = Technician.objects.get(id=pk)
            for key, value in content.items():
                setattr(technician, key, value)
            technician.save()
            return JsonResponse(
                technician,
                encoder=TechnicianModelEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Technician with given id does not exist."})
        except json.JSONDecodeError:
            return JsonResponse({"message": "Invalid JSON content."})
        except Exception as e:
            return JsonResponse(str(e))
@require_http_methods(["GET", "POST"])
def list_appointments(request):
    if request.method == "GET":
        try:
            appointments = Appointment.objects.all()
            appointments_list = list(appointments.values())
            return JsonResponse(
                {"appointments": appointments_list},
                encoder=AppointmentModelEncoder,
                safe=False,
            )
        except Technician.DoesNotExist:
            return JsonResponse({"message": "Error serializing content."})
        except Exception as e:
            return JsonResponse({"message": str(e)})
    else:
        try:
            content = json.loads(request.body)
            appointment = Appointment.objects.create(**content)
            return JsonResponse(
                AppointmentModelEncoder().default(appointment),
                status=201,
                safe=False,
            )
        except Exception as e:
            return JsonResponse({"message": str(e)})
@require_http_methods(["GET", "DELETE", "PUT"])
def show_appointments(request, pk):
    appointment = Appointment.objects.get(id=pk)
    if request.method == "GET":
        try:
            appointment = Appointment.objects.get(id=pk)
            return JsonResponse(
                {"appointment": appointment},
                AppointmentModelEncoder().default(appointment),
                safe=False,
            )
        except Appointment.DoesNotExist:
            return HttpResponse(status=404)
        except json.JSONDecodeError:
            return HttpResponse(status=400)
        except Exception as e:
            return HttpResponse(status=500)
    elif request.method == "DELETE":
        try:
            appointment = Appointment.objects.get(id=pk)
            appointment.delete()
            return JsonResponse({"message": "Appointment successfully deleted."})
        except Appointment.DoesNotExist:
            return JsonResponse({"message": "Appointment does not exist"}, status=400)
    else:
        try:
            content = json.loads(request.body)
            appointment = Appointment.objects.get(id=pk)
            for key, value in content.items():
                setattr(appointment, key, value)
            appointment.save()
            return JsonResponse(
                {"message": "Appointment successfully updated."},
                AppointmentModelEncoder().default(appointment),
                safe=False,
            )
        except Appointment.DoesNotExist:
            return HttpResponse(status=404)
        except json.JSONDecodeError:
            return HttpResponse(status=400)
        except Exception as e:
            return HttpResponse(status=500)
