from django.db import models


# Create your models here.
class AutomobileVO(models.Model):
    import_href = models.CharField(max_length=100, unique=True)
    vin = models.CharField(max_length=100, unique=True)
    sold = models.BooleanField(default=False)

    def __str__(self):
        return self.vin


class Salesperson(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    employee_id = models.CharField(max_length=25)
    picture_url = models.URLField(null = True, blank = True)

    def __str__(self):
        return self.first_name + " " + self.last_name


class Customer(models.Model):
    first_name = models.CharField(max_length=50)
    last_name = models.CharField(max_length=50)
    address = models.CharField(max_length=100)
    phone_number = models.CharField(max_length=15)

    def __str__(self):
        return self.first_name + " " + self.last_name


class Sale(models.Model):
    price = models.FloatField()
    automobile = models.ForeignKey(AutomobileVO, related_name = "sales", on_delete=models.CASCADE, null=True)
    salesperson = models.ForeignKey(Salesperson, related_name = "sales", on_delete=models.CASCADE)
    customer = models.ForeignKey(Customer, related_name = "sales", on_delete=models.CASCADE)

    def __str__(self):
        return str(self.automobile) + " " + str(self.salesperson) + " " + str(self.customer) + " " + str(self.price)
