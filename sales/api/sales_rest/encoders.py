from common.json import ModelEncoder
from .models import Salesperson, Customer, Sale, AutomobileVO
from common.json import ModelEncoder
from .models import Customer


class AutomobileVOEncoder(ModelEncoder):
    model = AutomobileVO
    properties = ["import_href", "vin", "sold"]


class SalespersonEncoder(ModelEncoder):
    model = Salesperson
    properties = ["first_name", "last_name", "employee_id", "id", "picture_url"]


class CustomerEncoder(ModelEncoder):
    model = Customer
    properties = ["first_name", "last_name", "address", "phone_number", "id"]

class SaleListEncoder(ModelEncoder):
    model = Sale
    properties = ["price", "automobile", "salesperson", "customer", "id"]

    encoders = {
        "automobile": AutomobileVOEncoder(),
        "salesperson": SalespersonEncoder(),
        "customer": CustomerEncoder(),
    }

    def get_extra_data(self, o):
        return {"automobile": o.automobile.vin,
                "salesperson": o.salesperson.first_name + " " + o.salesperson.last_name,
                "customer": o.customer.first_name + " " + o.customer.last_name,
                "salesperson_employee_id" : o.salesperson.employee_id
                }
