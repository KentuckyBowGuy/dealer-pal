from django.urls import path
from sales_rest.views import api_list_sales, api_list_salesmen, api_list_customers, api_show_salesman, api_show_customer, api_show_sale


urlpatterns = [
    path("sales/", api_list_sales),
    path("salespeople/", api_list_salesmen),
    path("customers/", api_list_customers),
    path("salespeople/<int:salesperson_id>/", api_show_salesman),
    path("customers/<int:customer_id>/", api_show_customer),
    path("sales/<int:sale_id>/", api_show_sale),
]
